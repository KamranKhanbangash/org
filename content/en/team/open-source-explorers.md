---
title: Explorers
description: Grey Software's Open Source Explorers
category: Team
postion: 3
explorers:  
  - name: Arsala
    avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
    position: President
    github: https://github.com/ArsalaBangash
    gitlab: https://gitlab.com/ArsalaBangash
    linkedin: https://linkedin.com/in/ArsalaBangash
  - name: Jia Hong
    avatar: https://avatars.githubusercontent.com/u/47465704?s=400&u=9aa7cfe98ec20f7b33abf59e4a7dd1fa92b74b8f
    position: Explorer
    github: https://github.com/jiahongle
    gitlab: https://gitlab.com/jiahongle
    linkedin: https://linkedin.com/in/jiahongle
---

## Explorers

<team-profiles :profiles="explorers"></team-profiles>
